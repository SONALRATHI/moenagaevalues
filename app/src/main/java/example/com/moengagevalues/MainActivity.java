package example.com.moengagevalues;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;
import com.moengage.push.PushManager;

public class MainActivity extends AppCompatActivity {
    TextView mBtn_register;
    EditText mFname,mLname, mEmail,mAge,mGender;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getIds();

        mBtn_register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String first_name = mFname.getText().toString();
                String last_name = mLname.getText().toString();
                String email = mEmail.getText().toString();
                String gender = mGender.getText().toString().trim();
                int age=Integer.parseInt(mAge.getText().toString());


                PayloadBuilder builder = new PayloadBuilder();
                builder.putAttrString("FIRST NAME", first_name)
                        .putAttrString("LAST NAME", last_name)
                        .putAttrString("EMAIL", email)
                        .putAttrString("GENDER", gender)
                        .putAttrInt("AGE", age);
                MoEHelper.getInstance(getApplicationContext()).trackEvent("register", builder.build());
            }
        });

    }


    public void getIds(){
        mFname=(EditText)findViewById(R.id.fname);
        mLname=(EditText)findViewById(R.id.lname);
        mEmail=(EditText)findViewById(R.id.email);
        mGender=(EditText)findViewById(R.id.gender);
        mAge=(EditText)findViewById(R.id.age);
        mBtn_register=(TextView) findViewById(R.id.btn_register);

    }
}
